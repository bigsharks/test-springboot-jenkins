package com.anshark.testspringbootjenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@RequestMapping("/api")
public class TestSpringbootJenkinsApplication {

    @RequestMapping("/hello")
    String hello(){
        return "my -> TestSpringbootJenkins 123456 哈哈";
    }

    public static void main(String[] args) {
        SpringApplication.run(TestSpringbootJenkinsApplication.class, args);
    }

}
